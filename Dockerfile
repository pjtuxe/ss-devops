FROM node:19-alpine AS builder

WORKDIR /app

RUN apk add mc

COPY package*.json .
RUN npm install

COPY tsconfig.json .
COPY src/ .

RUN npm run build


FROM node:19-alpine AS runtime

WORKDIR /app

COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist

EXPOSE 3000
STOPSIGNAL SIGQUIT
CMD [ "node", "dist/main.js" ]
